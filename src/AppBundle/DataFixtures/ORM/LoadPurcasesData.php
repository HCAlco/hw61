<?php
/**
 * Created by PhpStorm.
 * User: aibek
 * Date: 30.05.18
 * Time: 21:10
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Purchase;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadPurcasesData extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager){
        $dishes = [];
        $dishes[] = $this->getReference(LoadDishesData::DISH_ONE);
        $dishes[] = $this->getReference(LoadDishesData::DISH_TWO);
        $dishes[] = $this->getReference(LoadDishesData::DISH_THREE);
        $dishes[] = $this->getReference(LoadDishesData::DISH_FOUR);
        $dishes[] = $this->getReference(LoadDishesData::DISH_FIVE);
        $dishes[] = $this->getReference(LoadDishesData::DISH_SIX);
        $dishes[] = $this->getReference(LoadDishesData::DISH_SEVEN);

        $purchases = [];
        for($i = 0; $i < 500; $i++){
            $purchases[$i] = new Purchase();
            $rand = rand(0, count($dishes)-1);
            $purchases[$i]
                ->setDish($dishes[$rand])
                ->setDate(new \DateTime('201'.rand(6,8).'-'.rand(1,10).'-'.rand(1,30).
                                              ' '.rand(8,20).':'.rand(0,59).':'.rand(0,59)));
            $manager->persist($purchases[$i]);
        }
        $manager->flush();

    }

    public function getDependencies()
    {
        return array(
            LoadDishesData::class,
        );
    }
}