<?php
/**
 * Created by PhpStorm.
 * User: aibek
 * Date: 30.05.18
 * Time: 20:50
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Dish;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadDishesData extends Fixture implements DependentFixtureInterface
{
    public const DISH_ONE = 'dish_one';
    public const DISH_TWO = 'dish_two';
    public const DISH_THREE = 'dish_three';
    public const DISH_FOUR = 'dish_four';
    public const DISH_FIVE = 'dish_five';
    public const DISH_SIX = 'dish_six';
    public const DISH_SEVEN = 'dish_seven';
    public const DISH_EIGHT = 'dish_eight';
    public const DISH_NINE = 'dish_nine';
    public const DISH_TEN = 'dish_ten';

    public function load(ObjectManager $manager)
    {
        $food = [
            'Блины с изюмом', 'Рис в рисе с рисом', 'Стейк, прожаренный на солнце',
            'Улитки в остром соусе', 'Мясо птеродактиля', 'Шпинат в чесночном соусе',
            'Рыба в кислосладком соусе', 'Цыпленок Генерала Цо',
            'Лапша Дан-Дан','Утка по Пекински'
        ];
        $institution1 = $this->getReference(LoadInstitutionData::INSTITUTION_ONE);
        $institution2 = $this->getReference(LoadInstitutionData::INSTITUTION_TWO);
        $dishes = [];
        for($i = 0; $i < count($food); $i++){
            $dishes[$i] = new Dish();
            $dishes[$i]->setTitle($food[$i])
                ->setPrice(100 + ($i * 20))
                ->setInstitution(rand(0,1) ? $institution1 : $institution2)
                ->setImage('image'.($i+1).'.jpg');
            $manager->persist($dishes[$i]);
        }
        $manager->flush();


        $this->addReference(self::DISH_ONE, $dishes[0]);
        $this->addReference(self::DISH_TWO, $dishes[1]);
        $this->addReference(self::DISH_THREE, $dishes[2]);
        $this->addReference(self::DISH_FOUR, $dishes[3]);
        $this->addReference(self::DISH_FIVE, $dishes[4]);
        $this->addReference(self::DISH_SIX, $dishes[5]);
        $this->addReference(self::DISH_SEVEN, $dishes[6]);
        $this->addReference(self::DISH_EIGHT, $dishes[1]);
        $this->addReference(self::DISH_NINE, $dishes[8]);
        $this->addReference(self::DISH_TEN, $dishes[9]);
    }

    public function getDependencies()
    {
        return array(
            LoadInstitutionData::class,
        );
    }
}