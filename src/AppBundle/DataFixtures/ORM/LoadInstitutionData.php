<?php
/**
 * Created by PhpStorm.
 * User: aibek
 * Date: 29.05.18
 * Time: 18:21
 */

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Institution;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadInstitutionData extends Fixture
{

    public const INSTITUTION_ONE = 'faiza';
    public const INSTITUTION_TWO = 'china_officer';
    public function load(ObjectManager $manager)
    {
        $institution1 = new Institution();
        $institution1
            ->setTitle('Фаиза')
            ->setBody('Какая-то кафешка')
            ->setImage('faiza.jpg');

        $manager->persist($institution1);

        $institution2 = new Institution();
        $institution2
            ->setTitle('Китайский офицер')
            ->setBody('Ресторан китайской традиционной кухни')
            ->setImage('chinaCafe.jpg');

        $manager->persist($institution2);

        $manager->flush();

        $this->addReference(self::INSTITUTION_ONE, $institution1);
        $this->addReference(self::INSTITUTION_TWO, $institution2);
    }

}