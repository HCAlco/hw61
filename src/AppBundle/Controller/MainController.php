<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Institution;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class MainController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $institutions = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Institution')
            ->findAll();
        $popularDishes = [];
        foreach ($institutions as $institution){
            $dishes = $this
                ->getDoctrine()
                ->getRepository('AppBundle:Dish')
                ->getPopularDishes($institution, 2);
            $popularDishes[] = [
                'institution' =>$institution,
                'dishes' => $dishes
            ];
        }
        return $this->render('@App/Main/index.html.twig', array(
            'popularDishes' => $popularDishes
        ));
    }

    /**
     * @Route("/institution/{id}", requirements={"id": "\d+"})
     * @Method({"GET", "PUT"})
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction(int $id)
    {
        $institution = $this->getDoctrine()->getRepository(Institution::class)->find($id);
        return $this->render('@App/Main/view.html.twig', array(
            'institution' => $institution
        ));
    }

}
